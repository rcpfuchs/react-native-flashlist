import {Text, TextProps, View} from './Themed';
import {FlashList} from "@shopify/flash-list";
import {Button, Checkbox, TextInput} from "react-native-paper";
import {StyleSheet} from "react-native";
import React from "react";

export enum MyFlashListType {
    Input,
    Checkbox,
    Separator,
}

export type MyFlashListProps = {
    data: MyFlashListData[];
    setData: (data: MyFlashListData[]) => void;
    setRefresh: (refresh: number) => void;
    refresh: number;
}

export type MyFlashListData = {
    id: number;
    title: string;
    value?: string;
    type: MyFlashListType;
    boolValue?: boolean;
}

export function MyFlashList(props: MyFlashListProps) {

    // force a reactive refresh to rerender the list
    const refreshTable = () => {
        props.setRefresh(Math.random());
    }

    const updateItemAtIndex = (index: number, item: MyFlashListData) => {
        console.log("Update item at index " + index, item);
        const temp = props.data;
        temp[index] = item;
        props.setData(temp);
        refreshTable();
    }

    const handleCheckBoxValue = (item : MyFlashListData, index: number) => {
        item.boolValue = !item.boolValue;
        updateItemAtIndex(index, item);
    };

    const handleTextInputValue = (item : MyFlashListData, index: number, newText:string) => {
        console.log("TextInput blur", newText);
        item.value = newText;
        updateItemAtIndex(index, item);
    }

    const InputItem = ({item, index}: { item: MyFlashListData, index: number }) => {
        console.log("Render item at index " + index);
        console.log(item);
        switch (item.type) {
            case MyFlashListType.Input:
                return (<View style={styles.view}>
                    <Text style={[styles.formElement, styles.myFormElement]}>{item.title} - {item.value}</Text>
                    <TextInput
                        style={[styles.formElement, styles.myFormElement]}
                        onBlur={ (args) => {
                            handleTextInputValue(item, index, args.nativeEvent.text);
                        } }
                        defaultValue={item.value}
                        onChangeText={ (text) => {
                            console.log(text);
                            handleTextInputValue(item, index, text);
                        } }
                    ></TextInput>
                    <Button style={[styles.formElement, styles.myFormElement]} icon="camera" mode="contained"
                            onPress={() => console.log('Pressed')}>
                        Foto
                    </Button>
                </View>);

            case MyFlashListType.Checkbox:
                return (<View style={[ styles.view, styles.viewNoMarginNoPadding]}>
                    <Checkbox.Item label={item.title} status={item.boolValue? 'checked' : 'unchecked'} onPress={() => { handleCheckBoxValue(item, index)
                    }}></Checkbox.Item>
                </View>)

            default:
                return (<View style={styles.view}><Text style={[styles.formElement, styles.myFormElement]}>Unknown View</Text></View>)
        }
    }

    return <FlashList
        data={props.data}
        renderItem={InputItem}
        estimatedItemSize={60}
        keyExtractor={(item: MyFlashListData) => {
          return item.id.toString();
        }}
        extraData={props.refresh}
    />;
}

const styles = StyleSheet.create({
    formElement: {
        marginBottom: 8
    },
    myFormElement: {
        marginLeft: 16,
        marginRight: 16,
    },
    view: {
        paddingTop: 8,
        marginTop: 8,
        paddingBottom:8,
        borderBottomColor: '#222',
        borderBottomWidth: 1,
    },
    viewNoMarginNoPadding: {
        paddingTop: 0,
        paddingBottom: 0,
        marginTop: 0,
        marginBottom: 0,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});