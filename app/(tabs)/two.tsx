import {Dimensions, StyleSheet} from 'react-native';

import { Text, View } from '../../components/Themed';
import {MyFlashListType, MyFlashList, MyFlashListData} from "../../components/MyFlashList";
import React from "react";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";

export default function TabTwoScreen() {

  const [refresh, setRefresh] = React.useState(0);

  const [data  , setData] = React.useState<MyFlashListData[]>([
    {
      id: uuidv4(),
      title: "Cat",
      type: MyFlashListType.Checkbox,
      boolValue: true
    },
    {
      id: uuidv4(),
      title: "Dog",
      type: MyFlashListType.Checkbox,
      boolValue: true
    },
    {
      id: uuidv4(),
      title: "Chicken",
      type: MyFlashListType.Checkbox,
      boolValue: true
    },
    {
      id: uuidv4(),
      title: "Second Item",
      type: MyFlashListType.Input,
      value: "Hasenwurst"
    },
    {
      id: uuidv4(),
      title: "Second Item",
      type: MyFlashListType.Separator,
      value: "Hasenwurst"
    },
  ]);

  // add 100 items to data array
  for (let i = 0; i < 100; i++) {
    data.push({
      id: uuidv4(),
      title: "Item " + i,
      type: MyFlashListType.Checkbox,
      boolValue: true
    });
  }

  return (
      <View>
        <Text>Hallo: {data[0].boolValue? "Ja" : "Nein"}</Text>
        <Text>Hallo: {data[3].value? data[3].value : ""}</Text>
        <View style={{ height: Dimensions.get("screen").height-188, width: Dimensions.get("screen").width }}>
          <MyFlashList data={data} setData={ (data : MyFlashListData[] ) => { setData(data); } } refresh={refresh} setRefresh={ setRefresh } />
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  formElement: {
    marginBottom: 10
  },
  view: {
    padding: 20,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
